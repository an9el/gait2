##' Find the directory where the data lives
##'
##' @title Find directories in any allowed machine
##' @param data character with the data to search directory
##' @param external logical, if we want to find the data in the external drive
##' @param baseDir character to find the external drive, if external = TRUE
##' @return character with the folder's name where the requested data lives
##' @author person("Angel", "Martinez-Perez",
##' email = "angelmartinez@protonmail.com",
##' role = c("aut", "cre"),
##' comment = c(ORCID = "0000-0002-5934-1454"))
##' @keywords GAIT2,directory
##' @note This function without parameters return the available folders
##' @examples
##' mapDir <- g2.s.dirs('gwa.map.TOPMEDf5b')
##'
##' @seealso \code{g2.s.gwaGAIT2}
##'
##' @export
g2.s.dirs <- function(data, external = FALSE, baseDir = "/media/amartinezp/MyBook") {
    checkmate::assertFlag(external)

    if (missing(data)) {
        message("Available dirs:")
        message("--------------")
        message("* pheno     : phenotypes")
        message("* miRNA.map : map of miRNAs")
        message("* gwa           : 1000G imputation in RData format (for compatibility)")
        message("* gwa.1000G     : 1000G imputation in RData format")
        message("* gwa.1000G.vcf : 1000G imputation in vcf format")
        message("* gwa.1000G.gds : 1000G imputation in gds format")
        message("* gwa.map       : 1000G imputation map (for compatibility)")
        message("* gwa.map.1000G : 1000G imputation map")
        message("* gwa.HRC.vcf   : HRC imputation in vcf format, r1.1 2016 (GRCh37/hg19)")
        message("* gwa.HRC.gds   : HRC imputation in gds format, r1.1 2016 (GRCh37/hg19)")
        message("* gwa.1000G.michigan.vcf     : 1000G imputation in vcf format (GRCh37/hg19)")
        message("* gwa.1000G.michigan.gds     : 1000G imputation in gds format (GRCh37/hg19)")
        message("* gwa.TOPMEDr2.vcf           : TOPMEDr2 imputation in vcf format (GRCh38/hg38)")
        message("* gwa.TOPMEDr2.gds           : TOPMEDr2 imputation in gds format (GRCh38/hg38)")
        message("* gwa.TOPMEDf5b.vcf          : TOPMEDf5b imputation in vcf format (GRCh37/hg19)")
        message("* gwa.TOPMEDf5b.gds          : TOPMEDf5b imputation in gds format (GRCh37/hg19)")
        message("* gwa.TOPMEDf5b.gds.dosage   : TOPMEDf5b imputation in gds format, only dosage")
        message("* gwa.TOPMEDf5b.RData.dosage : TOPMEDf5b imputation in RData format, only dosage")
        message("* gwa.map.TOPMEDf5b     : TOPMEDf5b imputation map")
        message("* gwa.fine.mapping.ORM1 : fine mapping of ORM1 gen")
        message("* gwa.chipseps          : pre-imputed GWAs")
        message("* gwl.IBDs       : IBD matrices for autosomal chromosomes")
        message("* gwl.IBDs.map   : IBD map in hg38 for IBD matrices")
        message("* RNAseq      : genome wide expression")
        message("* RNAseq.map  : genome wide expression map")
        message("* kin2     : kinship matrix")
        message("* scripts  : source code folder")
        message("* res.gwa     : results of GWAs")
        message("* res.gwl     : results of GWL")
        message("* res.gwr     : results of GWR")
        message("* res.cor     : results of correlations")
        message("* res.h2r     : results of heritability")
        message("* res.summary : Summary document of GAIT2 project")
        message("* res.shiny   : Shiny application")
        message("* res.calls   : documentation of tele-calls and collaborations")
        message("--------------")
        message("parameters:")
        message("external = TRUE, for use the external disk drive")
        message("baseDir to change the folder of the external drive")

        return(invisible())
    }

    checkmate::assertCharacter(data,len = 1)
    if (external) {
    checkmate::assertCharacter(baseDir,len = 1)
       directorio <- switch(data,
         pheno = glue::glue("{baseDir}/datasets/GAIT2/g2.d.pheno"),
         miRNA.map = glue::glue("{baseDir}/datasets/GAIT2/g2.d.miRNA.map"),
         gwa = glue::glue("{baseDir}/datasets/GAIT2/g2.d.gwa/imputation_1000G"),
         gwa.1000G = glue::glue("{baseDir}/datasets/GAIT2/g2.d.gwa/imputation_1000G"),
         gwa.1000G.vcf = glue::glue("{baseDir}/datasets/GAIT2/g2.d.gwa/imputation_1000G_vcf"),
         gwa.1000G.gds = glue::glue("{baseDir}/datasets/GAIT2/g2.d.gwa/imputation_1000G_gds"),
         gwa.1000G.michigan.vcf = glue::glue("{baseDir}/datasets/GAIT2/g2.d.gwa/imputation_1000G_michigan_vcf"),
         gwa.1000G.michigan.gds = glue::glue("{baseDir}/datasets/GAIT2/g2.d.gwa/imputation_1000G_michigan_gds"),
         gwa.map = glue::glue("{baseDir}/datasets/GAIT2/g2.d.gwa.map"),
         gwa.map.1000G = glue::glue("{baseDir}/datasets/GAIT2/g2.d.gwa.map"),
         gwa.HRC.vcf = glue::glue("{baseDir}/datasets/GAIT2/g2.d.gwa/imputation_HRC"),
         gwa.HRC.gds = glue::glue("{baseDir}/datasets/GAIT2/g2.d.gwa/imputation_HRC_gds"),
         gwa.TOPMEDr2.vcf = glue::glue("{baseDir}/datasets/GAIT2/g2.d.gwa/imputation_TOPMED_r2"),
         gwa.TOPMEDr2.gds = glue::glue("{baseDir}/datasets/GAIT2/g2.d.gwa/imputation_TOPMED_r2_gds"),
         gwa.TOPMEDf5b.vcf = glue::glue("{baseDir}/datasets/GAIT2/g2.d.gwa/imputation_TOPMED_freeze5"),
         gwa.TOPMEDf5b.gds = glue::glue("{baseDir}/datasets/GAIT2/g2.d.gwa/imputation_TOPMED_freeze5_gds"),
         gwa.TOPMEDf5b.gds.dosage = glue::glue("{baseDir}/datasets/GAIT2/g2.d.gwa/imputation_TOPMED_freeze5_gds"),
         gwa.TOPMEDf5b.RData.dosage = glue::glue("{baseDir}/datasets/GAIT2/g2.d.gwa/imputation_TOPMED_freeze5_RData"),
         gwa.map.TOPMEDf5b = glue::glue("{baseDir}/datasets/GAIT2/g2.d.gwa.map"),
         gwa.fine.mapping.ORM1 = glue::glue("{baseDir}/datasets/GAIT2/g2.d.gwa/fine_mapping_ORM1"),
         gwa.chipseps = glue::glue("{baseDir}/datasets/GAIT2/g2.d.gwa/pre_imputation_2_chips"),
         gwl.IBDs = glue::glue("{baseDir}/DUMPS/angel/g2.d.gwl/g2.d.gwl.IBDs"),
         gwl.IBDs.map = glue::glue("{baseDir}/DUMPS/angel/g2.d.gwl/g2.d.gwl.1000G.map.hg38"),
         RNAseq = glue::glue("{baseDir}/datasets/GAIT2/g2.d.RNAseq"),
         RNAseq.map = glue::glue("{baseDir}/datasets/GAIT2/g2.d.RNAseq.map"),
         kin2 = glue::glue("{baseDir}/datasets/GAIT2/g2.d.pheno"),
         scripts = glue::glue("{baseDir}/datasets/GAIT2/g2.s"),
         res.gwa = glue::glue("{baseDir}/results/g2.r.gwa"),
         res.gwl = glue::glue("{baseDir}/results/g2.r.gwl"),
         res.gwr = glue::glue("{baseDir}/results/g2.r.gwa.region"),
         res.cor = glue::glue("{baseDir}/results/g2.r.cor"),
         res.h2r = glue::glue("{baseDir}/results/g2.r.h2r"),
         res.summary = glue::glue("{baseDir}/results/g2.r.summary"),
         res.shiny = glue::glue("{baseDir}/results/shiny-server"),
         res.calls = glue::glue("{baseDir}/results/g2.r.colaboraciones"))
    } else {
        nodename <- Sys.info()[["nodename"]]
        switch(nodename,
               debian = {
                   directorio <- switch(data,
                       pheno = "/home/datasets/GAIT2/g2.d.pheno",
                       miRNA.map = "/home/datasets/GAIT2/g2.d.miRNA.map",
                       gwa = "/home/datasets/GAIT2/g2.d.gwa/imputation_1000G",
                       gwa.1000G = "/home/datasets/GAIT2/g2.d.gwa/imputation_1000G",
                       gwa.1000G.vcf = "/home/datasets/GAIT2/g2.d.gwa/imputation_1000G_vcf",
                       gwa.1000G.gds = "/home/datasets/GAIT2/g2.d.gwa/imputation_1000G_gds",
                       gwa.1000G.michigan.vcf = "/home/datasets/GAIT2/g2.d.gwa/imputation_1000G_michigan_vcf",
                       gwa.1000G.michigan.gds = "/home/datasets/GAIT2/g2.d.gwa/imputation_1000G_michigan_gds",
                       gwa.map ="/home/datasets/GAIT2/g2.d.gwa.map",
                       gwa.map.1000G ="/home/datasets/GAIT2/g2.d.gwa.map",
                       gwa.HRC.vcf = "/home/datasets/GAIT2/g2.d.gwa/imputation_HRC",
                       gwa.HRC.gds = "/home/datasets/GAIT2/g2.d.gwa/imputation_HRC_gds",
                       gwa.TOPMEDr2.vcf = "/home/datasets/GAIT2/g2.d.gwa/imputation_TOPMED_r2",
                       gwa.TOPMEDr2.gds = "/home/datasets/GAIT2/g2.d.gwa/imputation_TOPMED_r2_gds",
                       gwa.TOPMEDf5b.vcf = "/home/datasets/GAIT2/g2.d.gwa/imputation_TOPMED_freeze5",
                       gwa.TOPMEDf5b.gds = "/home/datasets/GAIT2/g2.d.gwa/imputation_TOPMED_freeze5_gds",
                       gwa.TOPMEDf5b.gds.dosage = "/home/datasets/GAIT2/g2.d.gwa/imputation_TOPMED_freeze5_gds",
                       gwa.TOPMEDf5b.RData.dosage = "/home/datasets/GAIT2/g2.d.gwa/imputation_TOPMED_freeze5_RData",
                       gwa.map.TOPMEDf5b = "/home/datasets/GAIT2/g2.d.gwa.map",
                       gwa.fine.mapping.ORM1 = "/home/datasets/GAIT2/g2.d.gwa/fine_mapping_ORM1",
                       gwa.chipseps = "/home/datasets/GAIT2/g2.d.gwa/pre_imputation_2_chips",
                       gwl.IBDs = "/home/amartinezp/gait2/g2.d.gwl/g2.d.gwl.IBDs",
                       gwl.IBDs.map = "/home/amartinezp/gait2/g2.d.gwl/g2.d.gwl.1000G.map.hg38",
                       RNAseq = "/home/datasets/GAIT2/g2.d.RNAseq",
                       RNAseq.map = "/home/datasets/GAIT2/g2.d.RNAseq.map",
                       kin2 = "/home/datasets/GAIT2/g2.d.pheno",
                       scripts = "/home/datasets/GAIT2/g2.s",
                       res.gwa = "/home/results/g2.r.gwa",
                       res.gwl = "/home/results/g2.r.gwl",
                       res.gwr = "/home/results/g2.r.gwa.region",
                       res.cor = "/home/results/g2.r.cor",
                       res.h2r = "/home/results/g2.r.h2r",
                       res.summary = "/home/results/g2.r.summary",
                       res.shiny = "/home/results/shiny-server",
                       res.calls = "/home/results/g2.r.colaboraciones")
               },
               salambo2 = {
                   directorio <- switch(data,
                       pheno = "/home/amartinezp/gitlab/g2.phen.class/data",
                       scripts = "/home/amartinezp/Sync/lib/g2.s")
               },
               stop("*** Unknown machine ***")
               )
    }
    if (is.null(directorio)) {
        warning("No data found")
        return(invisible())
    } else {
        return(directorio)
    }
}
