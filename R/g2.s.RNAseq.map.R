
##' RNAseq.map with the gen map positions
##'
##' Only available in certain computers. It was enrich with
##' "R package org.Hs.eg.db, Version 3.4.0". Last update in 20170821
##' @title load the RNAseq.map of GAIT2
##' @return RNAseq.map data.frame with the RNAseq map of GAIT2
##'
##' \item{ENSEMBL}{gene id in ENSEMBL format}
##' \item{chr}{chromosome}
##' \item{gene}{gene id in original format}
##' \item{SYMBOL}{gene id in symbol format}
##' \item{descript}{Sort description of the name of the gene}
##' @author person("Angel", "Martinez-Perez",
##' email = "angelmartinez@protonmail.com",
##' role = c("aut", "cre"),
##' comment = c(ORCID = "0000-0002-5934-1454"))
##' @keywords GAIT2,RNAseq,map
##' @examples
##' map.RNA <- g2.s.RNAseq.map()
##'
##' @seealso \code{g2.s.RNAseq}
##'
##' @export
g2.s.RNAseq.map <- function(...) {
    file.map <- glue::glue("{g2.s.dirs("RNAseq.map", ...)}/g2.d.RNAseq.map.RData")
    checkmate::assertFileExists(file.map, access = "r", extension = "RData")
    map <- getobj(file.map)
    return(map)
}
