##' load the GWAS data of GAIT2
##'
##' load the genotype data of the GAIT2.
##' For TOPMED we retrieve the entire chromosome
##' @title g2.s.gwa
##' @param chr numeric chromosome to retrieve, 1-22
##' @param imputation The imputation for retrieve
##' @param type character of the file type to retrieve, fst and RData allowed
##' @param snps only for type='fst'. NULL for all snps in the chromosome,
##' or the "rsIDref" of the desired variants to retrieve
##' @return a data.frame with the values of the genetic variants for all
##' the subject in the GAIT2 study
##' @author person("Angel", "Martinez-Perez",
##' email = "angelmartinez@protonmail.com",
##' role = c("aut", "cre"),
##' comment = c(ORCID = "0000-0002-5934-1454"))
##' @keywords GAIT2, GWAS
##' @note Time to load all chr22, 151k snps = 33seg. Time to load chr2,
##' 907k snps = 5.6min. Time to load 8 arbitrary snps in chr2 = 0.8seg.
##' ALL times measured while system was copying files.
##' @examples
##' map <- g2.s.gwa.map(chr='22',
##' attributes = c('rsIDref', 'chromosome', 'position'))
##' require(dplyr)
##' algunos <- map %>% filter(position>33790000, position < 33800000) %>%
##' select(rsIDref) %>% unlist
##' chr22 <- g2.s.gwa(chr = '22', imputation = '1000G', snps = algunos)
##'
##' @seealso \code{g2.s.gwa.map}
##'
##' @export
g2.s.gwa <- function(chr,
                     imputation = "TOPMED",
                     type = "fst",
                     snps = NULL,
                     ...) {
    # check arguments
    checkmate::assertCharacter(chr, len = 1, min.chars = 1)
    stopifnot(chr %in% c(1:22, "X"))
    checkmate::assertCharacter(imputation, len = 1, min.chars = 1)
    if (!(imputation %in% c("TOPMED", "1000G"))) {
        warning("Not map for this imputation")
        return(invisible())
    }
    checkmate::assertCharacter(type, len = 1)

    if (imputation == "TOPMED") {
        file_aux <- file.path(g2.s.dirs("gwa.TOPMEDf5b.RData.dosage", ...),
                       paste0("g2.d.gwa.chr.", chr, ".TOPMEDf5b.dosage.RData"))
        checkmate::assertFileExists(file_aux, access = "r", extension = "RData")
        dat <- getobj(file_aux)
        if (!is.null(snps)) {
            checkmate::assertCharacter(snps, min.len = 1)
            kkk <- try(dat <- dat[snps, ], silent = TRUE)
            if (class(kkk) == "try-error") {
                warning("at least one SNP is not in this chromosome")
                return(invisible())
            }
        }
    }else if (imputation == "1000G") {
        stopifnot(chr %in% as.character(1:22))

        if (type == "fst") {
            stopifnot(require(fst, warn.conflicts = FALSE, quietly = TRUE))
            file_aux <- file.path(g2.s.dirs("gwa.1000G", ...),
                                 paste0("imputed.chr", chr, ".fst"))
            checkmate::assertFileExists(file_aux,
                                        access = "r",
                                        extension = "fst")
              if (is.null(snps)) {
                dat <- read_fst(file_aux)
            }else{
                checkmate::assertCharacter(snps, min.len = 1)
                kkk <- try(dat <- read_fst(file_aux, columns = c("id", snps)),
                           silent = TRUE)
                if (class(kkk) == "try-error") {
                    warning("at least one SNP is not in this chromosome")
                    return(invisible())
                }
            }
        }else if (type == "RData") {
            if (!is.null(snps))
                warning("snps parameter ignored")

            file_aux <- file.path(g2.s.dirs("gwa.1000G", ...),
                                 paste0("imputed.chr", chr, ".RData"))
            checkmate::assertFileExists(file_aux,
                                        access = "r",
                                        extension = "RData")
            dat <- getobj(file_aux)
        }else{
            warning("type not allowed")
            return(invisible())
        }
    }
    return(dat)
}
