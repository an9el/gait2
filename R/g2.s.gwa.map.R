##' load into memory the map of the genetic variants in the
##' chromosome specified, with the attributes specified
##'
##' The rsIDref is the key that is unique
##' @title g2.s.gwa.map
##' @param chr The chromosome of the map to return
##' @param imputation The imputation for retrieve the map
##' (type and atributes only available for 1000G imputation)
##' @param type character of the file type to retrieve,
##' fst and RData allowed
##' @param attributes Only for type='fst'. One of the
##' following: c("SNP", "rsIDref", "type", "source", "afr.aaf",
##' "amr.aaf", "asn.aaf", "eur.aaf", "afr.maf", "amr.maf",
##' "asn.maf", "eur.maf", "snpKey", "snpID", "snp", "rsID",
##' "position", "alleleA", "alleleB", "chromosome", "exp_maf",
##' "info", "LOCATION", "GENEID", "GENESYMBOL", "GENEdescript")
##' @return a data.frame with the required attributes of the map
##' @author person("Angel", "Martinez-Perez",
##' email = "angelmartinez@protonmail.com",
##' role = c("aut", "cre"), comment = c(ORCID = "0000-0002-5934-1454"))
##' @keywords GAIT2, GWAS, map
##' @import checkmate
##'
##' @examples
##' chr22 <- g2.s.gwa.map(chr = '22',
##' attributes = c('rsIDref', 'chromosome', 'position',
##' 'LOCATION', 'info', 'GENESYMBOL'))
##'
##' @seealso \code{g2.s.gwa.}
##'
##' @export
g2.s.gwa.map <- function(chr,
                         imputation = "1000G",
                         type = "fst",
                         attributes = c("SNP", "rsIDref", "chromosome",
                                        "position",
                                        "exp_maf", "info", "LOCATION", "GENEID",
                                        "GENESYMBOL", "GENEdescript"),
                         ...) {
    # check arguments
    checkmate::assertCharacter(chr, len = 1, min.chars = 1)
    stopifnot(chr %in% c(1:22, "X"))
    checkmate::assertCharacter(imputation, len = 1, min.chars = 1)
    if (!(imputation %in% c("TOPMED", "1000G"))) {
        warning("Not map for this imputation")
        return(invisible())
    }
    checkmate::assertCharacter(type, len = 1)
    checkmate::assertCharacter(attributes, min.len = 1)

    if (imputation == "TOPMED") {
        mapfile <- file.path(g2.s.dirs("gwa.map.TOPMEDf5b", ...),
                             paste0("g2.d.gwa.map.chr.", chr,
                                    ".TOPMEDf5b.dosage.RData"))
        stopifnot(file.exists(mapfile))
        snp_map <- getobj(mapfile)
    } else if (imputation == "1000G") {
        if (!(chr %in% 1:22)) {
            warning("chrX is not imputed with 1000G")
            return(invisible())
        }
        if (type == "fst") {
            stopifnot(require(fst, warn.conflicts = FALSE, quietly = TRUE))
            mapfile <- file.path(g2.s.dirs("gwa.map.1000G", ...),
                                 paste0("imputed.chr", chr,
                                        ".gwastools.snp.map.fst"))
            stopifnot(file.exists(mapfile))

            if (is.null(attributes)) {
                snp_map <- fst::read_fst(mapfile)
            } else {
                attributes2 <- intersect(
                    attributes,
                    c("SNP", "rsIDref", "type", "source", "afr.aaf",
                      "amr.aaf", "asn.aaf", "eur.aaf", "afr.maf", "amr.maf",
                      "asn.maf", "eur.maf", "snpKey", "snpID", "snp", "rsID",
                      "position", "alleleA", "alleleB", "chromosome",
                      "exp_maf", "info", "LOCATION", "GENEID", "GENESYMBOL",
                      "GENEdescript"))
                if (length(attributes2) != length(attributes))
                    warning("Some attributes may be misspelled.
Returning only existing attributes")
                snp_map <- read_fst(mapfile, columns = attributes2)
            }
        } else if (type == "RData") {
            if (!is.null(attributes))
                warning("attributes parameter ignored")

            mapfile <- file.path(g2.s.dirs("gwa.map", ...),
                                 paste0("imputed.chr", chr,
                                        ".gwastools.snp.map.RData"))
            stopifnot(file.exists(mapfile))
            snp_map <- getobj(mapfile)
        } else {
            warning("type not allowed")
            return(invisible())
        }
    }
    return(snp_map)
}
