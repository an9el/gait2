GAIT2vignette
================
Angel Martinez-Perez
2024-10-01

<!-- README.md is generated from README.Rmd. Please edit that file -->

------------------------------------------------------------------------

# gait2

<!-- badges: start -->

[![Lifecycle:
deprecated](https://img.shields.io/badge/lifecycle-deprecated-orange.svg)](https://www.tidyverse.org/lifecycle/#deprecated)
[![Last-changedate](https://img.shields.io/badge/last%20change-2024--10--01-yellowgreen.svg)](/commits/master)
<!-- badges: end -->

\[\[*TOC*\]\]

Package’s goal is to access the different type of data in the GAIT2
project. This package is superseded by [an9elproject
package](https://gitlab.com/an9el/an9elproject)

# Installation

You can install the released version of *gait2* package from
[gitlab](https://gitlab.com) with:

``` r
remotes::install_gitlab("an9el/gait2")
```

# Normal workflow with traits:

This function load the phenotypes of GAIT2 project into memory. Actually
it detects the computer from which is launched and load the data in the
correct folder. Also a parameter `external = TRUE` are allowed if we
have the data in a external drive with the same structure.

``` r
library(gait2)
library(checkmate)
# Load the phenotypes GAIT2
f1 <- g2.s.pheno()
```

Look for categories of phenotypes to work with

``` r
tail( f1$infoCategories() )
```

Look for the subcategories of the interesting category

``` r
tail( f1$infoCategories("funcion") )
```

Look for all the traits in this category

``` r
tail( f1$getCategories("funcion") )
```

get the traits of the category we are interested

``` r
f1$getTraits("fn.erythropathology")
```

Set the traits to work with:

``` r
traits <- c(f1$getTraits("solar"), f1$getTraits("covariates"), f1$getTraits("fn.erythropathology"))
f1$setTraits(traits)
```

summary of the data

``` r
f1$descriptives(c("VT", "AT", "FXIc"))
```

Little summary of the object:

``` r
f1$display()
```

Summary in list format

``` r
head(f1$summary())
```

Retrieve the data

``` r
dt <- f1$getData()
```

### Looking for the traits

One function to assert if a trait exist and, if not, returns error, is:

``` r
f1$assertTrait("venous")
```

Is this trait exist? return `TRUE` or `FALSE`

``` r
f1$existTrait("venous")
```

No, then, with this function we can discover traits that contains this
string in their names, labels or descriptions.

``` r
f1$findTraits("venous")[, 1:3]
```

### Calculate the residual of a numeric trait

We can use `g2.s.pheno.residualice` function to calculate the residual
or the normaliced residual of a numeric trait using `solarius` with the
covariates stored previously. See the help of this function for more
information.

### Plot one family to see the structure

``` r
f1$plot.pedigree("fam32")
```

# Workflow with SNPs

Lets retrieve the map of the chr21 in the TOPMED imputation

``` r
map <- head( g2.s.gwa.map(imputation = "TOPMED", chr = "21"))
```

Lets retrieve the genotypes in the TOPMED imputation

``` r
dat.snp <- g2.s.gwa(chr = "21", imputation = "TOPMED", snps = map$id)
dat.snp[, 1:4]
rm(dat.snp)

## Also access direct the gds files with:

genos <- g2.s.gwa.1000G.gds(chr = 21L,
      subject.sel = as.integer(1:8),
      snps.id = as.integer(15:25),
      filter = NULL,
      external = FALSE)
genos.map <- g2.s.gwa.1000G.gds(chr = 21L,
      subject.sel = as.integer(1:8),
      snps.id = as.integer(15:25),
      filter = NULL,
      external = FALSE,
      returnMAP = TRUE)
```

Last function `g2.s.gwa.1000G.gds` allow to retrieve subgrups of subject
or genetic variants (named by position or by name), also can extract
only that variants that pass certain filters. The function without
arguments display and immediate help

### Retrieve the kinship matrix

- g2.s.kin2()

# Workflow with RNAseq

Se the help of these function \* g2.s.RNAseq \* g2.s.RNAseq.map

# Workflow with miRNA

- g2.s.miRNA -\> this actually is consider a phenotype, so is a category
  in the f1 object
- g2.s.miRNA.map

# Miscellany

- normalizar -\> To make the normal transformation of one phenotype
- getobj -\> To retrieve an object stored in a .RData file
- g2.s.dirs -\> With this functions we can find the directory where the
  actual data are. Also where the results are stored
- locate.pheno -\> function to match characters
