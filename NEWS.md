# gait2 0.5.0

* New function `g2.s.pheno.residualice` to calculate directly the residual of a numeric trait given the significant covariates
* Update the *README.md* to include examples of new functions in the phenotype object of GAIT2, like `f1$findTraits` and `f1$assertTrait`
* Added a new function `g2.s.gwa.gds` to extract genotypes from TOPMEDr2 imputation. This function need to be tested and completed. Now only DS format is available and do not retrieve the variants from chrX.
* remove `normalizar` function, now is in `an9elutils` package
* pass lintr::lint function to the package

# gait2 0.4.0

* First version on `gitlab`
* Added support for `%>%` with `usethis::use_pipe()`
* Modified the *README.Rmd*
* Added folders to *IBDs* matrices and results of Linkage `g2.r.gwl`
* Added HRC imputation folder in HRC r1.1 2016 (GRCh37/hg19)
* Added 1000G imputation with Michigan imputation server (GRCh37/hg19)

# gait2 0.3.2

* Adding function `g2.s.houseHold` to extract the houseHold Matrix

# gait2 0.3.1

* substitution of two functions (`g2.s.gwa.1000G.gds` and `g2.s.gwa.1000G.gds.mapear`) for one `g2.s.gwa.1000G.gds` with more functionalities. Use of build functions to extract data and use of filters before extraction to allow extract partial data. More more quick than extract all the chromosome and then filter. Same function to extract data and map with the difference of only one parameter `returnMAP`

# gait2 0.3.0

* Adding function to extract genotypes in different formats directly from gds files
* Adding function to extract map directly from gds files
* Add support in `g2.s.dirs` to work with the external disk drive, and added the help if the function don't have arguments
* Added the following packages as dependencies ( `glue`, `gdsfmt`, `Hmisc`, `WGCNA`)

# gait2 0.2.2

* New `NEWS.md` file, ie, this file, to keep track of changes
* Adding "##' @import checkmate" in one file header in order to
  allow functions in this package to be used. With this label roxigen2
  create automatically the NAMESPACE file with the @import. Then when 
  the package is loaded this goes to "packages loaded via NAMESPACE"

# gait2 0.2.1

* `checkmate` is now a dependency for better checking function arguments.
  Is intended to progressively used in all functions from now on.
* New `locate.pheno()` makes easy searching for patterns in a character vector.
  If the vector have missing values, it is not returned in the search. Also is
  possible to make exact searches.
