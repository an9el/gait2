% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/locate.pheno.R
\name{locate.pheno}
\alias{locate.pheno}
\title{Look for matches in a string vector}
\usage{
locate.pheno(
  string,
  pattern,
  ignore_case = TRUE,
  verbose = FALSE,
  fixed = FALSE
)
}
\arguments{
\item{string}{Input character vector where to search the pattern}

\item{pattern}{character pattern for the search}

\item{ignore_case}{logical to indicate if we should ignore the capital letters}

\item{verbose}{logical to display a little summary of the findings}

\item{fixed}{logical to find only exact matches}
}
\value{
logical vector with the same length as string with the matches
}
\description{
Function to look for matches in the input character vector
}
\details{
This function when match a pattern with a `NA` return `FALSE` in this position.
Allow partial match, ignoring capital letters or exact matches
}
\note{
Created on 2019-12-10
}
\examples{
a <- locate.pheno(string = colors(), pattern = 'oCOl', verbose = TRUE)

}
\author{
person("Angel", "Martinez-Perez", email = "angelmartinez@protonmail.com",
role = c("aut", "cre"), comment = c(ORCID = "0000-0002-5934-1454"))
}
